__author__ = 'Akbashev-AR'


import socket

class ChatServer:

    server_addr = None
    port = None
    sock = None
    conn = None
    client_addr = None

    def __init__(self, addr, port):
        self.server_addr = addr
        self.port = port

    def create(self):
        try:
            self.sock = socket.socket()
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.bind((self.server_addr, self.port))
            self.sock.listen(10)
            print("Socket has been binded at " + str(self.server_addr) + ":" + str(self.port))
            print("Socket listing")
            print("Server has been created at " + str(self.server_addr) + ":" + str(self.port))
        except(socket.error, socket.timeout) as e:
            print(e)

    def send_data(self, data):
        self.conn.send(data.encode("utf-8"))

    def recieve_conn(self):
        self.conn, self.client_addr = self.sock.accept()

    def conn_close(self):
        self.conn.close()

    def sock_close(self):
        self.sock.close()