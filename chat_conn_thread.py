__author__ = 'Admin7'

import time
from chat_conn_massive import ConnMassive
import chat_server
from chat_data_processor import DataProcessor
from authorization import DataBase
from threading import Lock

lock = Lock()       #Thread locker

class ThreadConnection():

    conn_massive = ConnMassive.conn_massive
    #chat_server = chat_server.ChatServer
    th_conn = chat_server.ChatServer.conn
    db = DataBase()
    i = 0

    def thread_connection(self, chsrv):
        self.conn_massive["conn_" + str(self.i)] = self.th_conn
        print("conn_massive updated:", self.conn_massive)
        data_processor = DataProcessor()
        chsrv.sendData('==== Please enter your login ====\n\r')
        try:
            while 1:
                data_processor.recieve_data(self.th_conn)
                login = data_processor.rcv_data
                print(login)
                print(type(login))
                self.db.get_login(login)
                if login == self.db.get_login(login):
                    chat_server.send_data("Welcome to chat!\n\r")
                    break
                else:
                    chat_server.send_data("Sorry! Wrong login or password!\n\r")
                    chat_server.send_data("==== Please enter your login ====\n\r")
        except ConnectionResetError as e:
            print(e)
        except OSError as e:
            print(e)
        chat_server.send_data("Connected (" + str(time.strftime("%a, %d %b %Y %H:%M:%S")) + ")\n\r")
        try:
            while 1:
                data_processor.recieve_data(th_conn)
                if len(data_processor.rcv_data) == 0:
                    print("Client", chat_server.client_addr, "has left chat")
                    break
                data_processor.send_data_all(data_processor.rcv_data + "\n", th_conn)
                print(data_processor.rcv_data)
        except ConnectionResetError as e:
            print(e)
        except OSError as e:
            print(e)
        chat_server.conn_close()