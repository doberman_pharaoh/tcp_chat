__author__ = 'Admin7'


from chat_server import ChatServer
from chat_conn_thread import ThreadConnection
from threading import Thread

addr = "127.0.0.1"
port = 3333
i = 0               #Thread name iterator
th_conn = None      #Thread client connection
login = None

chat_server = ChatServer(addr, port)
chat_server.create()
def t(chsrv):
    ThreadConnection().thread_connection(chsrv)

while 1:
    chat_server.recieve_conn()
    thread = Thread(name="Th_" + str(i), target=t(), kwargs={'chsrv': chat_server}).start()
    i = i + 1
    print("Client address:", chat_server.client_addr, "has connected to Chat")

chat_server.conn_close()
chat_server.sock_close()