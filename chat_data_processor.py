__author__ = 'Admin7'

from chat_conn_massive import ConnMassive

class DataProcessor:
    data = None
    rcv_data = None

    def recieve_data(self, conn):
        try:
            self.data = conn.recv(1024)
            self.rcv_data = self.data.decode("utf-8")
        except UnicodeDecodeError as e:
            print(e)
            self.rcv_data = "Unicode decode error has occured when connected"
        except ConnectionResetError as e:
            print(e)
        except OSError as e:
            print(e)

    def send_data_all(self, data, th_conn):
        for key, val in ConnMassive.conn_massive.items():
            if val != th_conn:
                val.send(data.encode("utf-8"))