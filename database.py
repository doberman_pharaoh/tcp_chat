__author__ = 'user'

import sqlite3

class DatabaseConnectionHandler:

    def connect(self):
        self.conn = sqlite3.connect("tcpchat.db")
        self.c = self.conn.cursor()

    def add(self, login, password):
        self.c.execute('''CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT)''')
        self.c.execute("INSERT INTO users (login, password) VALUES (?, ?)", (login, password))
        self.conn.commit()

    def get_login(self, login):
        result = self.c.execute('SELECT login FROM users WHERE login=?', [login])
        for row in result.fetchall():
            print(row)

    def get_all(self):
        result = self.c.execute('SELECT * FROM users')
        for row in result.fetchall():
            print(row)

    def clear_DB(self):
        self.c.execute('DELETE FROM users')
        self.conn.commit()

    def close(self):
        self.conn.close()


db = DatabaseConnectionHandler()

db.connect()
#db.add("putty1", "123")
#db.add("putty2", "321")
db.get_login('putty1')
#db.clear_DB()
db.get_all()
db.close()